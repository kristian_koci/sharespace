# Sharespace minimal code 

Quindi, su questo micro progetto, ce', nell'archivio **models.py** il conditional che ho usato, se l'UserAddress, quindi, se e' la prima volta che si crea, per impostazione predefinita, l'oggetto contenente tutti i fields obbligatori del modelo, questa e' stata una mia idea, perche sarebbe come l'object minimo.
Nel caso, ci fosse **full_address** e l'oggetto sia in modo "editing" (**self.pk**) allora salva tutto il resto e sovrascrive l'oggetto precedente. Non l'ho testato in full, perche ho altri test da fare, e mi hai commentato che era piu per capire il mio approccio, quindi, penso che sia un approccio abbastanza semplice ed effettivo.

Nel caso dell'IBAN, ho creato il modello nel **fields.py**, ce' un piccolo helper (**helpers.py**) per validare il checksum,  e la lunghezza dello string.
Vi e' anche utilizzato il **@deconstructible** decorator che permette al sistema Django di fare le migrazioni con questo nuovo campo.
