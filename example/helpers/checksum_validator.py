from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ImproperlyConfigured, ValidationError
                            
@deconstructible
class IBANValidator():
    @staticmethod
    def iban_checksum(value):
        value = value[4:] + value[:2] + '00'
        value_digits = ''
        for x in value:
            if '0' <= x <= '9':
                value_digits += x
            elif 'A' <= x <= 'Z':
                value_digits += str(ord(x) - 55)
            else:
                raise ValidationError(_('%s is an invalid character.') % x)

        return '%02d' % (98 - int(value_digits) % 97)

    def __call__(self, value):

        if value is None:
            return value

        value = value.upper().replace(' ', '').replace('-', '')

        if self.iban_checksum(value) != value[2:4]:
            raise ValidationError(_('Invalid IBAN.'))