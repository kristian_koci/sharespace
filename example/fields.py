from django.db import models
from django.utils.translation import ugettext_lazy as _
from .helpers.checksum_validator import IBANValidator

class IBANField(models.CharField):

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('max_length', 34)
        self.default_validators = [IBANValidator()]
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        value = super().to_python(value)
        if value in self.empty_values:
            return self.empty_value
        return value.upper().replace(' ', '').replace('-', '')

    def prepare_value(self, value):
        if value is None:
            return value
        grouping = 4
        value = value.upper().replace(' ', '').replace('-', '')
        return ' '.join(value[i:i + grouping] for i in range(0, len(value), grouping))