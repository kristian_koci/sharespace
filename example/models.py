from django.db import models
from .fields import IBANField
from django.contrib.auth.models import User
# Create your models here.

class MyIBAN(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    iban = IBANField()

    def __str__(self):
        return self.iban

 
class UserAddress(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=255)
    street_address = models.CharField(max_length=255)
    street_address_line2 = models.CharField(max_length=255, blank=True, null=True)
    zipcode = models.CharField(max_length=12, blank=True, null=True)
    city = models.CharField(max_length=64)
    state = models.CharField(max_length=64, blank=True, null=True)
    country = models.CharField(max_length=2)
    full_address = models.TextField(blank=True)
    
    def save(self, *args, **kwargs):
        basicdata = f"{self.name}\n{self.street_address} {self.city} {self.country}"
        if self.pk and self.full_address:
            self.full_address = f"{self.basicdata}\n{self.street_address_line2} {self.zipcode} {self.state}"
        elif not self.pk:
            super().save(*args, **kwargs)